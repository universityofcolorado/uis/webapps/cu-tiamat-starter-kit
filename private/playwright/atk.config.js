/*
* Automated Testing Kit configuration.
*/
module.exports = {
  operatingMode: "native",
  drushCmd: "drush",
  registerUrl: "user/register",
  logInUrl: "user/login",
  logOutUrl: "user/logout",
  resetPasswordUrl: "user/password",
  contactUsUrl: "form/contact",
  authDir: "private/playwright/tests/support",
  dataDir: "private/playwright/tests/data",
  supportDir: "private/playwright/tests/support",
  testDir: "private/playwright/tests",
  pantheon : {
    isTarget: false,
    site: "aSite",
    environment: "dev"
  }
}
